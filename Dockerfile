FROM openjdk:17-alpine
EXPOSE 8080
ADD target/SpringBootSimpleEksDeploy.jar helloworld
ENTRYPOINT ["java", "-jar","helloworld"]